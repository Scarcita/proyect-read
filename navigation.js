import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { NavigationContainer} from "@react-navigation/native";


///navegacion

import homeScreen from "./screens/homeScreen";
import rutinasScreen from "./screens/rutinasScreen";
import sesionesScreen from "./screens/sesionesScreen";

//import MaterialComunityIcons from 'react-native-vector-icons/MaterialComunityIcons';

const Tab = createBottomTabNavigator();

function MyTabs() {
  return (
    <Tab.Navigator
      initialRouteName="home"
      screenOptions={{
        tabBarActiveTintColor: "FFF843"
      }}>
      <Tab.Screen 
      name="Home" 
      component={homeScreen} />

      <Tab.Screen 
      name="Rutinas" 
      component={rutinasScreen}
      options={{
        tabBarLabel: 'Feed',
        headerShown: false,
      }}
      />

      <Tab.Screen 
      name="Sesiones" 
      component={sesionesScreen} 
      options={{
        tabBarLabel: 'Feed',
        headerShown: false,
      }}
      />
      
    </Tab.Navigator>

    );  
 
}

export default function navigation() {
  return (
    <NavigationContainer>
      <MyTabs/>
    </NavigationContainer>
  )
}